﻿PROJECT TITLE: Jas2Jas  
GROUP: 6B  

NAME1: Bruno Gomes, NR1: 070509137, GRADE1: 16, CONTRIBUTION: 25%  
NAME2: João Gonçalves, NR2: 090509136, GRADE2: 16, CONTRIBUTION: 25%  
NAME3: João Lopes, NR3: 100509009, GRADE3: 16, CONTRIBUTION: 25%  
NAME4: Ricardo Pinho, NR31: 090509045, GRADE4: 16, CONTRIBUTION: 25%  

** SUMMARY: A nossa ferramenta é um compilador de Jasmin para Jasmin, que tem a possibilidade de incluir pontos de monitorização no código, que podem ser fornecidos através de um ficheiro XML. 
O comando para executar o programa é:
java -jar jas2jas.jar <ouput> <inject>
<output> - nome do ficheiro.jas
<inject> - campo opcional para um xml com código para injetar no resultado

**DEALING WITH SYNTACTIC ERRORS: Existe um Try Catch que verifica erros sintáticos, devolvendo uma mensagem predefinida bem como informaçao relevante sobre o erro.

SEMANTIC ANALYSIS: Inicialmente é criada uma tabela de simbolos ao gerar a arvore sintatica de alto nivel, porém não são feitas verificações semânticas.  

**INTERMEDIATE REPRESENTATIONS (IRs): Construção de uma arvore de baixo nível onde quarda tudo o que recebe do ficheiro, 
verificando apenas erros sintanticos e gramaticais. 
De seguida existe um Parser que guarda a informaçao noutra arvore anotada para verificações semanticas, 
construção da tabela de simbolos e geração de codigo.   

**CODE GENERATION: A geração de Código é feita através dos campos guardados em cada Nó guardada que são chamados recursivamente, acrescentando cada dado a uma estrutura StringBuilder.
Não foi verificada para todas as instruções, logo é possível que existam algumas para o qual o parse não é feito e 
posterior geração de código.  

**OVERVIEW: A análise gramatical e Sintática é feita através de Tokens declarados no ficheiro jjt, obedecendo a verificações de expressões regulares.
São criados Nodes por cada Token existente no ficheiro, onde, após a construção de uma arvore de baixo nível,
e feito o parse da informaçao aqui guardada, organizando a informação em classes específicas para o tipo de cada
instrução, nomeadamente, ClassNode que corresponde a classes, MethodNode, que corresponde a métodos, etc.
Com essa construção, são também guardados os dados essenciais na tabela de símbolos, nomeadamente nomes , tipos e valores de
variáveis globais, nomes e tipos de retorno de métodos, entre outros.Após a construção desta árvore, É feita a geração
de código, onde todas as instruções guardam a sua informação num StringBuilder, acrescentando cada nó a este até terminar
a árvore. Por conseguinte, é criado um ficheiro onde é guardada a informação desta estrutura, sendo este o ficheiro .jas
compilado. Além disto, existe uma estrutura implementada para testes em junit, bem como uma estrutura para parse de Xml e
injecção de código.

**TESTSUITE AND TEST INFRASTRUCTURE: Existe um package onde são feitos asserts em relação aos ficheiros gerados e aos ficheiros experados, onde a análise é feita linha por linha.

**TASK DISTRIBUTION: Cada elemento do grupo contribuiu para cada uma das fases do projecto.

**PROS: Geração de Código funcional e abrangente. Verificações gramaticais e sintáticas completas. 

**CONS: Problemas na Injecção de código. Análise semântica limitada.