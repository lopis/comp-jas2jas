package nodes;

public class TableRow {

	public String type;
	public String value;
	
	public TableRow (String type, String value)
	{
		this.type=type;
		this.value=value;
	}
	public TableRow (String type)
	{
		this.type=type;
	}
	public TableRow ()
	{
	}
}
