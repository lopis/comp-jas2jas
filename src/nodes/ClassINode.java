package nodes;


public class ClassINode extends InstructionNode {
	
	public String classname;
	public String name;

	
	
	public ClassINode(String name, String _classname){
		classname=_classname;
		this.name=name;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+classname);
		sb.append("\n");
	}

}

