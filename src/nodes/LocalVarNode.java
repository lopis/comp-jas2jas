package nodes;


public class LocalVarNode extends InstructionNode {
	
	public String varnum;
	public String value;
	public String name;

	
	
	public LocalVarNode(String name, String _varnum, String value){
		this.name=name;
		varnum=_varnum;
		this.value = value;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
		TableRow temp = new TableRow(varnum,value);
		localvarsTable.put(name, temp);
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+varnum);
		sb.append("\n");
	}

}

