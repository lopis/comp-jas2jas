package nodes;

public class FieldManipulationNode extends InstructionNode {
	
	public String fieldspec;
	public String descriptor;
	public String name;

	
	
	public FieldManipulationNode(String name, String fieldspec, String descriptor){
		this.name=name;
		this.fieldspec=fieldspec;
		this.descriptor=descriptor;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+fieldspec+" "+descriptor);
		sb.append("\n");
	}

}
