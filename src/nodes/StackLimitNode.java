package nodes;

public class StackLimitNode extends DirectiveNode {
	
	public final String type = "stack";
	public int value;

	public StackLimitNode(int _value){
		value=_value;
	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(".limit");
		sb.append(" " + type+" "+ value);
		sb.append("\n");
	}

}
