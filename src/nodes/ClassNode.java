package nodes;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import utils.*;

public class ClassNode extends Jas2JasNode {
	
	/**
	 * Class node attributes.
	 */
	public String className;
	public String visibility;
	public ParserXml xml;
	public static HashMap<String,TableRow> fieldTable = new HashMap<String,TableRow>();
	public static HashMap<String,TableRow> methodTable = new HashMap<String,TableRow>();

	/**
	 * Class childs.
	 */
	public SuperNode superChildNode;
	public LinkedList<FieldNode> fieldNodes = new LinkedList<FieldNode>();
	public LinkedList<MethodNode> methodNodes = new LinkedList<MethodNode>();

	
	/**
	 * Constructor.
	 * @param className
	 * @param visibility
	 */
	public ClassNode(String className, String visibility) {
		this.className = className;
		this.visibility = visibility;
	}
	
	public ClassNode() {}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		// add method to symboltable
		// child.buildSymbolTable();
		// table.addClass(className);
		TableRow temp = new TableRow();
		classTable.put(className, temp);
		
	}
	
	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		//child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(".class");
		sb.append(" "+ visibility);
		sb.append(" " + this.className);
		sb.append("\n");
		if(superChildNode != null) 
			superChildNode.generateCode();
		for(FieldNode field : fieldNodes){
		   field.generateCode();
		}
		
		if(xml != null) {
			for (Monitor monitor : xml.getMonitors()) {
				for (MonitorClass moclass : monitor.getClasses()) {
					if(moclass.getName().equals(className)){
						for (String meth : moclass.getMethods()) {
							for (MethodNode methodNode : methodNodes) {
								if(methodNode.getMethodName().equals(meth)){
									sb.append(".field public " + methodNode.getMethodName() + "counter I = 0\n");
									methodNode.setInjectNumCalls();
								}
							}
						}
					} else {
						System.err.println("No injection.");
					}
				}
			}
//			for(MethodNode methodNode : methodNodes){
//				if(Monitor.get(methodNode.getMethodName()) != null) {
//					sb.append("field public " + methodNode.getMethodName() + "counter I = 0");
//					methodNode.setInjectNumCalls();
//				}
//			}
		}
		
		
		for(MethodNode methodNode : methodNodes){
			methodNode.generateCode();
		}
	}
	
	/**
	 * Add a SuperNode as a child.
	 * @param superChildNode
	 */
	public void addSuperChild(SuperNode superChildNode){
		this.superChildNode = superChildNode;
	}
	
	/**
	 * Add a FieldNode as a child.
	 * @param fieldChildNode
	 */
	public void addFieldChild(FieldNode fieldChildNode){
		this.fieldNodes.add(fieldChildNode);
	}
	
	/**
	 * Add a MethodNode as a child.
	 * @param methodChildNode
	 */
	public void addMethodChild(MethodNode methodChildNode){
		methodChildNode.setClassName(className);
		this.methodNodes.add(methodChildNode);
	}

}