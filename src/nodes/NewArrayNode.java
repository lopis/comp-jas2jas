package nodes;

public class NewArrayNode extends InstructionNode {
	public String arraytype;
	public String name;

	
	
	public NewArrayNode(String name, String arraytype){
		this.arraytype=arraytype;
		this.name = name;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+arraytype);
		sb.append("\n");
	}

}
