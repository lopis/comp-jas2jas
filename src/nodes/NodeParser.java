package nodes;

import java.util.LinkedList;

import jas2jas.Jas2Jas;
import jas2jas.SimpleNode;

public class NodeParser {
	
	public static ClassNode classNode;
	public static LinkedList<String> errorStack = new LinkedList<String>();
	public static LinkedList<String> warningStack = new LinkedList<String>();

	public static ClassNode parse(SimpleNode tree){
		if(tree.nodeName != "JasminClass"){
			System.err.println("SINTAX ERROR: Expected JasminClass node. Encountered "
					+ tree.nodeName + ".");
			return null;
		} else {
			System.out.println(" = Tree Parsing = ");
			parseClass(tree);
			System.out.println(" = = = =  = = = = ");
		}
		return classNode;
	}

	/**
	 * A jasmin class includes the super directive, field directives
	 * and methods. The methods node includes several methods which
	 * must be parsed. 
	 * @param tree A JasminClass node
	 */
	private static void parseClass(SimpleNode tree) {
		// First node must be ClassDirective node
		System.out.println(((SimpleNode) tree.jjtGetChild(0)).nodeName);
		classNode = parseClassDirective((SimpleNode) tree.jjtGetChild(0));
		
		// The following nodes are .super, .limit and .method nodes
		for (int i = 1; i < tree.jjtGetNumChildren(); i++) {
			String nodeName = ((SimpleNode) tree.jjtGetChild(i)).nodeName;
//			System.out.println(nodeName);
			if(nodeName.equals("SuperDirective")){
				parseSuperDirective((SimpleNode) tree.jjtGetChild(i));
			}
			else if(nodeName.equals("FieldDirective")){
				parseFieldDirective((SimpleNode) tree.jjtGetChild(i));
			}
			else if(nodeName.equals("MethodNode")){
				parseMethod((SimpleNode) tree.jjtGetChild(i));
			}
		}
	}

	/**
	 * Parses the methods node. This node includes directives (limits, vars..)
	 * and instruction. Terminates with ".end method".
	 */
	private static void parseMethod(SimpleNode node) {
		// First node must be MethodDirective node
		System.out.println("parseMethods: " + node.nodeName);
		
		MethodNode method = parseMethodDirective((SimpleNode) node.jjtGetChild(0));
		
		for (int i = 1; i < node.jjtGetNumChildren(); i++) {
			// Parse each method's inscrutions
			if(node.jjtGetSimpleChild(i).nodeName == "LimitDirective"){
				DirectiveNode limit = parseLimit(node.jjtGetSimpleChild(i));
				method.addLimitChild(limit);
			} else if(node.jjtGetSimpleChild(i).nodeName == "Instruction"){
				InstructionNode instNode = parseInstruction(node.jjtGetSimpleChild(i));
				method.addInstructionChild(instNode);
			}
			
		}
		classNode.addMethodChild(method);
	}

	private static DirectiveNode parseLimit(SimpleNode jjtGetSimpleChild) {
		DirectiveNode limit = new StackLimitNode(2);
		return limit;		
	}

	private static InstructionNode parseInstruction(SimpleNode node) {
		InstructionNode instruction;
		
		int instType = node.jjtGetSimpleChild(0).instruction;
		int lineNum = node.jjtGetSimpleChild(0).lineNum;
		String instName = node.jjtGetSimpleChild(0).val;
		System.out.println(node.nodeName + " : " + node.jjtGetSimpleChild(0).val + " (" + node.jjtGetSimpleChild(0).instruction + ")");
		
		
		if(instType == Jas2Jas.NOPARAM_INST){
			instruction = new NoParamNode(instName);
		} else if(instType == Jas2Jas.LDC_INST){
			System.out.println("line num: " + lineNum);
			String constant = node.jjtGetSimpleChild(0).jjtGetSimpleChild(0).val;
			System.err.println(" ### LCD " + constant);
			instruction = new LdcNode(instName, constant);
		} else if(instType == Jas2Jas.BRANCH_INST){
			String labelname = node.jjtGetSimpleChild(0).jjtGetSimpleChild(0).val;;
			instruction = new BranchNode(instName, labelname);
		} else if(instType == Jas2Jas.METHOD_INST){
			String methodname = node.jjtGetSimpleChild(0).jjtGetSimpleChild(0).val;;
			instruction = new MethodINode(instName, methodname);
		} else if(instType == Jas2Jas.FIELD_INST){
			String fieldspec = node.jjtGetSimpleChild(0).jjtGetSimpleChild(0).val;;
			String descriptor = node.jjtGetSimpleChild(0).jjtGetSimpleChild(1).val;;
			instruction = new FieldManipulationNode(instName, fieldspec, descriptor);
		} else {
			errorStack.add("Sintax error: Unknown instruction " + node.jjtGetSimpleChild(0).val + ".");
			return null;
		}
		
		return instruction;
	}

	private static MethodNode parseMethodDirective(SimpleNode node) {
		String visibility = "";
		String methodName = "";
		String returnType = "";
		String params = "";
		
		for (int i = 0; i < node.children.length; i++) {
			String nodeName = ((SimpleNode)node.children[i]).nodeName;
			String val = ((SimpleNode)node.children[i]).val;
			
			if( nodeName.equals("Visibility") ){
				visibility += " " + val;
			} else if ( nodeName.equals("Url") ){
				String[] vals = val.split("[\u0028|\u0029]"); // parentheses
				methodName = vals[0];
				
				if(vals.length > 1)
					params = vals[1];
				if(vals.length > 2)
					returnType = vals[2];
			}
		}
		System.err.println("Method Directive: " + methodName + ", " + visibility.trim()
				+ ", " + returnType + ", " + params + "; ");
		return new MethodNode(methodName, params, returnType, visibility.trim());
	}

	/**
	 * Parses the field node in class.
	 * @param node
	 */
	private static void parseFieldDirective(SimpleNode node) {
		String visibility = "";
		String fieldName = "";
		String fieldType = "";
		String fieldValue = "";
		
		for (int i = 0; i < node.children.length; i++) {
			String nodeName = ((SimpleNode)node.children[i]).nodeName;
			String value = ((SimpleNode)node.children[i]).val;
			
			if( nodeName.equals("Visibility") ){
				visibility = value;
			} else if ( nodeName.equals("Label") ){
				fieldName = value;
			} else if ( nodeName.equals("TypeDescriptor") ){
				fieldType = ((SimpleNode)((SimpleNode)node.children[i]).jjtGetChild(0)).val;
			} else if ( nodeName.equals("Value") ){
				fieldValue = value;
			}
		}
		System.err.println("Field Directive: " + visibility + ", " + 
				fieldName + ", " + fieldType + ", " + fieldValue + ", ");
		FieldNode field = new FieldNode(fieldName, visibility, fieldType, fieldValue);
		classNode.addFieldChild(field);
		
	}

	/**
	 * Parses the super node in class
	 * @param node
	 */
	private static void parseSuperDirective(SimpleNode node) {
		String superURL = ((SimpleNode) node.jjtGetChild(0)).val;
		System.err.println("Super Directive: " + superURL);
		SuperNode scNode = new SuperNode(superURL);
		classNode.addSuperChild(scNode);
	}

	/**
	 * Parses the class directive node in class.
	 * @param node
	 * @return
	 */
	private static ClassNode parseClassDirective(SimpleNode node) {
//		boolean visibility = false;
//		boolean finalval = false;
//		boolean superval = false;
//		boolean interfaceval = false;
//		boolean abstractval = false;
		String visibility = "";
		String className = "";
		
		for (int i = 0; i < node.children.length; i++) {
			String nodeName = ((SimpleNode)node.children[i]).nodeName;
			
			if( nodeName.equals("Visibility") ){
				visibility += " " + ((SimpleNode)node.children[i]).val;
			} else if ( nodeName.equals("Label") ){
				className = ((SimpleNode)node.children[i]).val;
			}
		}
		System.err.println("new class: " + className + " " + visibility.trim());
		return new ClassNode(className, visibility.trim());
	}
}
