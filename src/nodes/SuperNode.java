package nodes;

public class SuperNode extends Jas2JasNode {
	
	public String className;

	public SuperNode(String className) {
		this.className = className;
	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		//child.verify();
		// Verificar se a classe existe?
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(".super");
		sb.append(" " + this.className + "\n");
	}
}