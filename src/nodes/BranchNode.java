package nodes;

public class BranchNode extends InstructionNode {
	
	public String labelname;
	public String name;

	
	
	public BranchNode(String name, String _labelname){
		this.name=name;
		labelname=_labelname;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+labelname + "\n");
	}

}
