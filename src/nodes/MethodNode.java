package nodes;

import java.util.HashMap;
import java.util.LinkedList;

public class MethodNode extends Jas2JasNode{
	
	private String methodName;
	private String returnType;
	private String visibility;
	private String params;
	private String className;
	boolean injectable = false;
	public HashMap<String,TableRow> localvarsTable = new HashMap<String,TableRow>();
	public LinkedList<InstructionNode> instructionNodes = new LinkedList<InstructionNode>();
	public LinkedList<DirectiveNode> limitNodes = new LinkedList<DirectiveNode>();
	

	public MethodNode(){}
	public MethodNode(String methodName, String params, String returnType, String visibility){
		this.methodName = methodName;
		this.returnType = returnType;
		this.visibility = visibility;
		this.params = params;
	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
		// table.addClass(className);
		TableRow temp = new TableRow(returnType);
//		methodTable.put(methodName, temp);
		String tempparams[] = params.split(" ");
		for(int i=0; i<tempparams.length;i++)
		{
			temp = new TableRow(tempparams[i]);
			localvarsTable.put(tempparams[i+1], temp);
			i++;
			if(i==tempparams.length)
			{
				//erro parametros mal declarados
			}
		}
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(".method");
		
		sb.append(" "+visibility);
		sb.append(" " + methodName);
		sb.append("(" + params+")");
		
		sb.append("" + returnType);
		sb.append("\n");
		
		if(injectable){
			sb.append("\n.var 0 is this L" + className + "; from Numcalls0 to Numcalls1"
					+ "\nNumcalls0:" +
							"\naload_0" +
							"\ndup" +
							"\ngetfield "+ className + "/" + methodName + "counter I" +
							"\niconst_1" +
							"\niadd" +
							"\nputfield "+ className + "/" + methodName + "counter I" +
							"\nNumcalls1:\n");
		}
			
		for(DirectiveNode limit : limitNodes){
			limit.generateCode();
		}
		for(InstructionNode instruction : instructionNodes){
			instruction.generateCode();
		}
		
		sb.append(".end method\n");
	}
	
	public void addInstructionChild(InstructionNode instructionChildNode){
		instructionNodes.add(instructionChildNode);
	}
	
	public void addLimitChild(DirectiveNode limitChildNode){
		limitNodes.add(limitChildNode);
	}
	public void setInjectNumCalls() {
		this.injectable = true;
		
	}
	public String getMethodName() {
		
		return this.methodName;
	}
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}

}