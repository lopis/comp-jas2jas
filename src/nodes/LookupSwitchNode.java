package nodes;

public class LookupSwitchNode extends InstructionNode {

	public String name;
	
	
	public LookupSwitchNode(String name){
		this.name=name;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		//sb.append(" "+constant);
		sb.append(name);
		sb.append("\n");
	}
}
