package nodes;

public class MethodINode extends InstructionNode {
	
	public String methodname;
	public String name;
	

	
	
	public MethodINode(String name,String _methodname){
		methodname=_methodname;
		this.name=name;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+methodname);
		sb.append("\n");
	}

}
