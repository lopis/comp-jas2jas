package nodes;

public class IntegerNode extends InstructionNode {
	
	public int integerParam;
	public String name;

	
	
	public IntegerNode(String name,int _integerParam){
		this.name=name;
		integerParam=_integerParam;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+integerParam);
		sb.append("\n");
	}

}
