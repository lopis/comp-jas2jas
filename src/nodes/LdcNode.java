package nodes;

public class LdcNode extends InstructionNode {
	public String constant;
	public String name;

	
	
	public LdcNode(String name, String constant){
		this.name=name;
		this.constant=constant;

	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		//table.addMethod(methodName, returnType, visibility);
		// add method to symboltable
		// child.buildSymbolTable();
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		// Return type must match the method signature
		// child.verify();
	}

	@Override
	public void generateCode(){
		// Generate the code
		sb.append(name);
		sb.append(" "+constant);
		sb.append("\n");
	}
}
