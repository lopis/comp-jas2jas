package nodes;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public abstract class Jas2JasNode{
	public static StringBuilder sb = new StringBuilder();
	public static HashMap<String,TableRow> classTable = new HashMap<String,TableRow>();// symbol table of classes
	//public static Table symbolTable = new Table();
	public Jas2JasNode child;
	
	abstract void buildSymbolTable();

	abstract void verifySemantics();
	
	abstract void generateCode();
	
	public void addChild(Jas2JasNode child){
		this.child = child;
	}
	
	public void toFile(String fileName){
		try {
			FileWriter fstream = new FileWriter(fileName);
			BufferedWriter buf = new BufferedWriter(fstream);
			buf.write(sb.toString());
			buf.close();
			fstream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Constants
	 */
	public static final int PUBLIC = 1, PRIVATE = 2, PROTECTED = 3;
	public static final int EMPTY = 0, FINAL = 1, TRANSIENT = 2;
	public static final int VOID = 0, INTEGER = 1, FLOAT = 2, STRING = 3, DOUBLE = 4;

}