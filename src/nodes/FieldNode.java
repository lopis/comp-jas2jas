package nodes;

public class FieldNode extends ClassNode {
	
	public String fieldName;
	public String visibility;
	public String fieldType;
	public String fieldValue;


	public FieldNode(String fieldName, String visibility, String fieldType, String fieldValue) {
		this.fieldName = fieldName;
		this.visibility = visibility;
		this.fieldType = fieldType;
		this.fieldValue = fieldValue;
	}

	@Override
	public void buildSymbolTable(){
		// Walk the tree and get the symbol table
		// add method to symboltable
		// child.buildSymbolTable();
		// table.addClass(className);
		TableRow temp = new TableRow(fieldType,fieldValue);
		fieldTable.put(fieldName, temp);
		
	}

	@Override
	public void verifySemantics(){
		// Verify semantic rules and cross with symbol table
		//child.verify();
	}

	@Override
	public void generateCode(){
		sb.append(".field");
		/*switch(visibility){
			case PUBLIC:
				sb.append(" " + "public");
				break;
			case PRIVATE:
				sb.append(" " + "private");
				break;
			case PROTECTED:
				sb.append(" " + "protected");
				break;
			default:
				sb.append(" " + "public");				
		}
		sb.append(" " + this.fieldName);
		switch(fieldType) {
			case INTEGER:
				sb.append(" " + "I");
				break;
			case FLOAT:
				sb.append(" " + "F");
				break;
			case STRING:
				sb.append(" " + "Ljava/lang/String");
				break;
			case DOUBLE:
				sb.append(" " + "D");
				break;
			default:
				break;
		}*/
		sb.append(" " + this.visibility);
		sb.append(" " + this.fieldName);
		sb.append(" "+this.fieldType);
		sb.append(" = "+this.fieldValue);
		sb.append("\n");
		// TODO .. atribuicoes?
	}
}