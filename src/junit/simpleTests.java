package junit;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

public class simpleTests {

	public InputStream stream = new InputStream() {
		
		@Override
		public int read() throws IOException {
			return 0;
		}
	};
	
	@Test
	public void test1() {
		ReadFile f1 = new ReadFile("test1.jas");
		System.out.println(f1.getTree());
		try {
			BufferedReader input =  new BufferedReader(new FileReader(f1.getFileReaded()));
			String teststr=input.readLine();
			BufferedReader input2 =  new BufferedReader(new FileReader("Gentest1.jas"));
			String teststr2=input2.readLine();
			while (teststr!=null)
			{
				teststr=teststr.trim();
				assertEquals(teststr.equals(teststr2),true);
				teststr=input.readLine();
				teststr2=input2.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
	}
	
	@Test
	public void test2() {
		ReadFile f1 = new ReadFile("test2.jas");
		System.out.println(f1.getTree());
		try {
			BufferedReader input =  new BufferedReader(new FileReader(f1.getFileReaded()));
			String teststr=input.readLine();
			BufferedReader input2 =  new BufferedReader(new FileReader("Gentest2.jas"));
			String teststr2=input2.readLine();
			while (teststr!=null)
			{
				teststr=teststr.trim();
				if(teststr.contains(".class"))
					assertEquals(teststr.equals(teststr2),false);
				else
					assertEquals(teststr.equals(teststr2),true);
				teststr=input.readLine();
				teststr2=input2.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test3() {
		ReadFile f1 = new ReadFile("test3.jas");
		System.out.println(f1.getTree());
		try {
			BufferedReader input =  new BufferedReader(new FileReader(f1.getFileReaded()));
			String teststr=input.readLine();
			BufferedReader input2 =  new BufferedReader(new FileReader("Gentest3.jas"));
			String teststr2=input2.readLine();
			while (teststr!=null)
			{
				System.out.println(teststr);
				teststr=teststr.trim();
				assertEquals(teststr.equals(teststr2),true);
				teststr=input.readLine();
				teststr2=input2.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test4() {
		ReadFile f1 = new ReadFile("test4.jas");
		System.out.println(f1.getTree());
		try {
			BufferedReader input =  new BufferedReader(new FileReader(f1.getFileReaded()));
			String teststr=input.readLine();
			BufferedReader input2 =  new BufferedReader(new FileReader("Gentest4.jas"));
			String teststr2=input2.readLine();
			while (teststr!=null)
			{
				teststr=teststr.trim();
				assertEquals(teststr.equals(teststr2),true);
				teststr=input.readLine();
				teststr2=input2.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test5() {
		ReadFile f1 = new ReadFile("test5.jas");
		System.out.println(f1.getTree());
		try {
			BufferedReader input =  new BufferedReader(new FileReader(f1.getFileReaded()));
			String teststr=input.readLine();
			BufferedReader input2 =  new BufferedReader(new FileReader("Gentest5.jas"));
			String teststr2=input2.readLine();
			while (teststr!=null)
			{
				teststr=teststr.trim();
				assertEquals(teststr.equals(teststr2),true);
				teststr=input.readLine();
				teststr2=input2.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test6() {
		ReadFile f1 = new ReadFile("test6.jas");
		System.out.println(f1.getTree());
		try {
			BufferedReader input =  new BufferedReader(new FileReader(f1.getFileReaded()));
			String teststr=input.readLine();
			BufferedReader input2 =  new BufferedReader(new FileReader("Gentest6.jas"));
			String teststr2=input2.readLine();
			while (teststr!=null)
			{
				teststr=teststr.trim();
				assertEquals(teststr.equals(teststr2),false);
				teststr=input.readLine();
				teststr2=input2.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
