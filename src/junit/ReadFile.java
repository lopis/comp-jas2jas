package junit;

import jas2jas.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class ReadFile {
	
	private FileInputStream fis ;
	private String inputFilePath;
	private Jas2Jas jasmin ;
	private SimpleNode jasminRoot;
	private File fileReaded;
	private String tree;
	private String code;
	


	public ReadFile(String inputFilePath){
		this.inputFilePath = inputFilePath;

	       try {

	    	 this.fis = new FileInputStream(inputFilePath);
	    	 this.jasmin = new Jas2Jas (this.fis);
		     this.jasminRoot = jasmin.JasminClass();
		     this.fileReaded = new File(inputFilePath);

	       } catch (FileNotFoundException | ParseException e) {

	           e.printStackTrace();
	       }	   
	       
	}


	public SimpleNode getJasminRoot() {
		return jasminRoot;
	}


	public void setJasminRoot(SimpleNode jasminRoot) {
		this.jasminRoot = jasminRoot;
	}


	public FileInputStream getFis() {
		return fis;
	}

	public void setFis(FileInputStream fis) {
		this.fis = fis;
	}


	public String getInputFilePath() {
		return inputFilePath;
	}


	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}


	public Jas2Jas getJasmin() {
		return jasmin;
	}


	public void setJasmin(Jas2Jas jasmin) {
		this.jasmin = jasmin;
	}


	public File getFileReaded() {
		return fileReaded;
	}


	public void setFileReaded(File fileReaded) {
		this.fileReaded = fileReaded;
	}


	public String getTree() {
		return tree;
	}


	public void setTree(String tree) {
		this.tree = tree;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}

	

}
