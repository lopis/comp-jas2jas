package utils;

import java.util.Vector;

public class Monitor {

	private String monitorType;
	private Vector<MonitorClass> classes;
	
	
	public Monitor(String monitorType){
		this.monitorType =  monitorType;
		classes = new Vector<MonitorClass>();
	}
	
	public void addClass(MonitorClass c){
		this.classes.add(c);
	}

	public String getMonitorType() {
		return monitorType;
	}

	public void setMonitorType(String monitorType) {
		this.monitorType = monitorType;
	}

	public Vector<MonitorClass> getClasses() {
		return classes;
	}

	public void setClasses(Vector<MonitorClass> classes) {
		this.classes = classes;
	}
	
}
