package utils;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParserXml {
	
	private Vector<Monitor> monitors;
	private File file;
	private String filePath;
	
	public ParserXml(String path){
		this.filePath = path;
		file = new File(path);
		this.monitors = parseXML();
		
	}
	
	public ParserXml(File file){
		monitors = new Vector<Monitor>();
		this.file = file;
		this.monitors = parseXML();
		
	}

	public Vector<Monitor> parseXML() {
		Vector<Monitor> monitors_aux = new Vector<Monitor>();
		Element	fstElement;
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			NodeList rootNodeL = doc.getElementsByTagName("jas2jas");
			Node rootNode = rootNodeL.item(0);
			//System.out.println(rootNode.getNodeName());
			fstElement = (Element) rootNode;
			
			
			NodeList monitorL = doc.getElementsByTagName("monitor");
			
			//System.out.println(monitorL.getLength());
			
			for(int i = 0; i<monitorL.getLength(); i++){ 
				
				Node typeNode = monitorL.item(i);
				fstElement = (Element) typeNode;
				NodeList nfl = fstElement.getElementsByTagName("type");
				Element ef = (Element) nfl.item(0);
				NodeList nodelist = ef.getChildNodes();
				String value = ((Node) nodelist.item(0)).getNodeValue(); //
				//System.err.println(value);
				Monitor m = new Monitor(value);
				
				NodeList classList = fstElement.getElementsByTagName("class");
				//System.out.println("->>>>" + classList.getLength());
				for(int h= 0; h< classList.getLength(); h++){
					
					Element classN = (Element) classList.item(h);
					NodeList classNameL = classN.getElementsByTagName("className");
					Element className = (Element) classNameL.item(0);
					MonitorClass cl = new MonitorClass(className.getTextContent());
					
					
					NodeList classFunctionL = classN.getElementsByTagName("function");
					
					for(int k = 0; k< classFunctionL.getLength(); k++){
						Element function = (Element) classFunctionL.item(k);
						cl.addMethod( function.getTextContent());
					}
					m.addClass(cl);
				
				}
				
				monitors_aux.add(m);			
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return monitors_aux;
	}

	public Vector<Monitor> getMonitors() {
		return monitors;
	}

	public void setMonitors(Vector<Monitor> monitors) {
		this.monitors = monitors;
	}
	
	

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
}
