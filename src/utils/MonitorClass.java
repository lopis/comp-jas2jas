package utils;

import java.util.Vector;

public class MonitorClass {
	
	private String name;
	private Vector<String> methods;
	
	
	public MonitorClass(String name){
		this.name = name;
		methods = new Vector<String>();
		
	}
	
	public void addMethod( String methodName){
		this.methods.add(methodName);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<String> getMethods() {
		return methods;
	}

	public void setMethods(Vector<String> methods) {
		this.methods = methods;
	}
	
	
}
